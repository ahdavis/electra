/*
 * TileProps.ts
 * Defines the properties of a Tile component
 * Created on 6/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * The properties of a [[Tile]] component
 */
export interface TileProps {
	/**
	 * The x-coordinate of the [[Tile]] 
	 * relative to the top left corner
	 * of the app window, measured in
	 * pixels
	 */
	xPos: number;

	/**
	 * The y-coordinate of the [[Tile]]
	 * relative to the top left corner
	 * of the app window, measured in
	 * pixels
	 */
	yPos: number;

	/**
	 * The scaling factor of the [[Tile]],
	 * as a percentage
	 */
	scale: number;
}

//end of file
