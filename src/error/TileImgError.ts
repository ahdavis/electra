/*
 * TileImgError.ts
 * Defines an error that is thrown when a bad tile image ID is found
 * Created on 6/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { TileImgID, nameForImgID } from '../tile/TileImgID';

/**
 * Thrown when a bad [[TileImgID]] is found
 */
export class TileImgError extends Error {
	//no fields

	/**
	 * Constructs a new TileImgError instance
	 *
	 * @param badID The bad [[TileImgID]]
	 */
	constructor(badID: TileImgID) {
		super('Bad tile image ID: ' + nameForImgID(badID));
	}
}

//end of file
