/*
 * Window.tsx
 * Defines a React component that represents a window
 * Created on 6/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { WindowState } from '../states/WindowState';
import { WindowProps } from '../props/WindowProps';

/**
 * The window the app runs in
 */
export class Window extends React.Component<WindowProps, WindowState> {
	//no fields

	/**
	 * Constructs a new Window component
	 *
	 * @param props The properties of the Window
	 */
	constructor(props: WindowProps) {
		super(props) //call the superclass constructor
	}

	/**
	 * Renders the Window
	 *
	 * @returns The React DOM for the Window
	 */
	public render(): React.ReactNode {
		return (
			<h1>Hello World!</h1>
		);
	}
}

//end of file
