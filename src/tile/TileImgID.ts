/*
 * TileImgID.ts
 * Enumerates tile image IDs for Electra
 * Created on 6/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * The ID of a [[Tile]] image
 */
export enum TileImgID {
	Blank,
	Grass,
	Mountains,
	Trees,
	Water,
	MaxImgVal
}

/**
 * Returns the name associated with an image ID
 *
 * @param id The ID to get the name for
 *
 * @returns The name for the ID
 */
export function nameForImgID(id: TileImgID): string {
	switch(id) {
		case TileImgID.Blank: {
			return 'Blank';
		}
		case TileImgID.Grass: {
			return 'Grass';
		}
		case TileImgID.Mountains: {
			return 'Mountains';
		}
		case TileImgID.Trees: {
			return 'Trees';
		}
		case TileImgID.Water: {
			return 'Water';
		}
		case TileImgID.MaxImgVal: {
			return 'Error';
		}
	}
}

//end of file
