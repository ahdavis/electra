/*
 * TileImg.ts
 * Defines a class that represents a tile image
 * Created on 6/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { TileImgID } from './TileImgID';
import { TileImgError } from '../error/TileImgError';

/**
 * The image for a [[Tile]]
 */
export class TileImg {
	//fields
	/**
	 * The ID for the image
	 */
	private _id: TileImgID;

	/**
	 * The path to the image file
	 */
	private _path: string;

	//methods

	/**
	 * Constructs a new `TileImg` instance
	 *
	 * @param newID the ID of the image to load
	 */
	constructor(newID: TileImgID) {
		this._id = newID;
		this._path = this.determinePath();
	}

	/**
	 * Gets the ID of the `TileImg`
	 *
	 * @returns The ID of the `TileImg`
	 */
	public get id(): TileImgID {
		return this._id;
	}

	/**
	 * Gets the path to the image
	 * represented by the `TileImg`
	 *
	 * @returns The path to the represented image
	 */
	public get path(): string {
		return this._path;
	}

	/**
	 * Determines the path to the image file
	 * represented by the `TileImg`
	 *
	 * @returns The path to the image file
	 */
	private determinePath(): string {
		//get the base path for the app
		let retPath = '../';

		//append the path to the tiles directory
		retPath += 'assets/images/tiles/';

		//append the filename
		switch(this._id) {
			case TileImgID.Grass: {
				retPath += 'grass.png';
				break;
			}
			case TileImgID.Mountains: {
				retPath += 'mountains.png';
				break;
			}
			case TileImgID.Trees: {
				retPath += 'trees.png';
				break;
			}
			case TileImgID.Water: {
				retPath += 'water.png';
				break;
			}
			case TileImgID.Blank: {
				retPath += 'blank.png';
				break;
			}
			default: { //unknown tile ID
				throw new TileImgError(this._id);
			}
		}

		//and return the assembled path
		return retPath;
	}
}

//end of file
