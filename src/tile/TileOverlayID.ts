/*
 * TileOverlayID.ts
 * Enumerates tile overlay IDs for Electra
 * Created on 6/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * The ID of a [[Tile]] overlay
 */
export enum TileOverlayID {
	None,
	City,
	Dungeon,
	MaxOverlayVal
}

/**
 * Returns the name for an overlay ID
 *
 * @param id The ID to get the name for
 *
 * @returns The name for the ID
 */
export function nameForOverlayID(id: TileOverlayID): string {
	switch(id) {
		case TileOverlayID.None: {
			return 'None';
		}
		case TileOverlayID.City: {
			return 'City';
		}
		case TileOverlayID.Dungeon: {
			return 'Dungeon';
		}
		case TileOverlayID.MaxOverlayVal: {
			return 'Error';
		}
	}
}

//end of file
