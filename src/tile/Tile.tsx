/*
 * Tile.tsx
 * Defines a React component that represents a map tile
 * Created on 6/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { TileProps } from '../props/TileProps';
import { TileState } from '../states/TileState';
import { TileImgID, nameForImgID } from './TileImgID';
import { TileImg } from './TileImg';
import { TileOverlayID, nameForOverlayID } from './TileOverlayID';
import { TileOverlay } from './TileOverlay';
import { remote } from 'electron';

/**
 * A map tile
 */
export class Tile extends React.Component<TileProps, TileState> {
	//no fields

	//methods

	/**
	 * Constructs a new `Tile` component
	 *
	 * @param props The properties of the `Tile`
	 */
	constructor(props: TileProps) {
		super(props); //call the superclass constructor

		//and init the state
		this.state = {
			imageID: TileImgID.Blank,
			overlayID: TileOverlayID.None
		};
	}

	/**
	 * Renders the `Tile`
	 *
	 * @returns A React DOM representing the `Tile`
	 */
	public render(): React.ReactNode {
		return (
			<button style={this.genButtonCSS()}
				onClick={() => 
					this.chooseTextureOrOverlay()}>
				{this.assembleDOM()}
			</button>
		);
	}

	/**
	 * Generates the CSS for the `Tile`'s button
	 *
	 * @returns the CSS for the `Tile`'s button
	 */
	private genButtonCSS(): React.CSSProperties {
		return {
			position: 'absolute',
			left: this.props.xPos.toString() + 'px',
			top: this.props.yPos.toString() + 'px',
			width: (64 * 
				(this.props.scale / 100.0)).toString() +
					'px',
			height: (64 *
				(this.props.scale / 100.0)).toString() +
					'px',
			margin: 0,
			padding: 0,
			border: 'none'
		};
	}

	/**
	 * Generates the CSS for the `Tile`'s images
	 *
	 * @param isOverlay Whether the image is an overlay
	 *
	 * @returns The CSS for the `Tile`'s image
	 */
	private genImageCSS(isOverlay: boolean): React.CSSProperties {
		//calculate the z-index for layering images
		let z: number;
		if(isOverlay) {
			z = 20;
		} else {
			z = 10;
		}

		//and generate the CSS
		return {
			position: 'absolute',
			top: '0px',
			left: '0px',
			zIndex: z,
			width: '100%',
			height: '100%',
			margin: 0,
			padding: 0,
			border: 'none'
		};
	}

	/**
	 * Allows the user to choose a new texture or overlay
	 * for the `Tile`
	 */
	private chooseTextureOrOverlay(): void {
		//assemble the array of choices
		let choices = [
			'Cancel',
			'Texture',
			'Overlay'
		];

		//create the option object for the choice box
		let options = {
			type: 'question',
			buttons: choices,
			defaultId: 1,
			title: 'Choose an option',
			message: 'Add an overlay or a texture?',
			cancelId: 0
		};

		//show the message box
		let res = remote.dialog.showMessageBox(options);

		//and handle the result
		if(res === 1) {
			this.chooseTexture();
		} else if(res === 2) {
			this.chooseOverlay();
		}
	}

	/**
	 * Allows the user to choose a texture for the `Tile`
	 */
	private chooseTexture(): void {
		//assemble the array of texture choices
		let choices: string[] = ['Cancel'];
		for(let i = TileImgID.Blank; 
			i < TileImgID.MaxImgVal; i++) {
			choices.push(nameForImgID(i));
		}

		//create the option object for the choice box
		let options = {
			type: 'question',
			buttons: choices,
			defaultId: 1,
			title: 'Texture Choice',
			message: 'Choose a texture for the map tile.',
			cancelId: 0
		};

		//show the message box and get the result
		let res = remote.dialog.showMessageBox(options);
		
		//and handle the result
		if(res !== 0) {
			this.setState({imageID: res - 1});
		}
	}

	/**
	 * Allows the user to choose an overlay for the `Tile`
	 */
	private chooseOverlay(): void {
		//assemble the array of overlay choices
		let choices: string[] = ['Cancel'];
		for(let i = TileOverlayID.None; 
			i < TileOverlayID.MaxOverlayVal; i++) {
			choices.push(nameForOverlayID(i));
		}

		//create the option object for the choice box
		let options = {
			type: 'question',
			buttons: choices,
			defaultId: 1,
			title: 'Overlay Choice',
			message: 'Choose an overlay for the map tile.',
			cancelId: 0
		};

		//show the message box and get the result
		let res = remote.dialog.showMessageBox(options);

		//and handle the result
		if(res !== 0) {
			this.setState({overlayID: res - 1});
		}
	}

	/**
	 * Generates the React DOM for the `Tile`'s images
	 *
	 * @returns The React DOM for the `Tile`
	 */
	private assembleDOM(): React.ReactNode {
		//declare the return object
		let ret: React.ReactNode[] = [];

		//create the texture DOM
		let tileImg = new TileImg(this.state.imageID);
		console.log(tileImg.path);
		ret.push(<img src={tileImg.path} 
				style={this.genImageCSS(false)} />);

		//create the overlay DOM
		if(this.state.overlayID !== TileOverlayID.None) {
			let tileOverlay = new TileOverlay(
						this.state.overlayID);
			ret.push(<img src={tileOverlay.path}
				style={this.genImageCSS(true)} />);
		}

		//and return the assembled DOM
		return ret;
	}
}

//end of file
