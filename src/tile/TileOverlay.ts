/*
 * TileOverlay.ts
 * Defines a class that represents a tile overlay
 * Created on 6/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { TileOverlayID } from './TileOverlayID';
import { TileOverlayError } from '../error/TileOverlayError';

/**
 * The overlay for a [[Tile]]
 */
export class TileOverlay {
	//fields
	/**
	 * The ID for the overlay
	 */
	private _id: TileOverlayID;

	/**
	 * The path to the overlay image file
	 */
	private _path: string;

	//methods

	/**
	 * Constructs a new `TileOverlay` instance
	 *
	 * @param newID the ID of the overlay to load
	 */
	constructor(newID: TileOverlayID) {
		this._id = newID;
		this._path = this.determinePath();
	}

	/**
	 * Gets the ID of the `TileOverlay`
	 *
	 * @returns The ID of the `TileOverlay`
	 */
	public get id(): TileOverlayID {
		return this._id;
	}

	/**
	 * Gets the path to the overlay image
	 * represented by the `TileOverlay`
	 *
	 * @returns The path to the represented overlay image
	 */
	public get path(): string {
		return this._path;
	}

	/**
	 * Determines the path to the image file
	 * represented by the `TileOverlay`
	 *
	 * @returns The path to the image file
	 */
	private determinePath(): string {
		//get the base path for the app
		let retPath = '../';

		//append the path to the tiles directory
		retPath += 'assets/images/overlays/';

		//append the filename
		switch(this._id) {
			case TileOverlayID.City: {
				retPath += 'city.png';
				break;
			}
			case TileOverlayID.Dungeon: {
				retPath += 'dungeon.png';
				break;
			}
			default: { //unknown tile overlay ID
				throw new TileOverlayError(this._id);
			}
		}

		//and return the assembled path
		return retPath;
	}
}

//end of file
