/*
 * webpack.react.config.js
 * Webpack React config file for Electra
 * Created on 6/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

//create the HTML plugin object
const htmlPlugin = new HtmlWebPackPlugin({
	template: "./src/index.html",
	filename: "./index.html"
});

//create the config object
const config = {
	target: "electron-renderer",
	devtool: "source-map",
	entry: "./src/app/renderer.tsx",
	output: {
		filename: "renderer.js",
		path: path.resolve(__dirname, "js")
	},
	module: {
		rules: [
			{
				test: /\.(ts|tsx)$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			}
		]
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js"]
	},
	plugins: [htmlPlugin]
};

//export the config object
module.exports = (env, argv) => {
	return config;
}

//end of file
